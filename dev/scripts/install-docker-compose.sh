#!/bin/bash
# Modified version of official docker-compose install script (https://docs.docker.com/compose/install/)
#Run this script as root

url="https://github.com/docker/compose/releases/download/1.11.2/docker-compose-$(uname -s)-$(uname -m)"
docker_compose_dest="/usr/local/bin/docker-compose"

curl -L ${url} -o ${docker_compose_dest}
chmod +x /usr/local/bin/docker-compose
