(function() {
  angular
      .module('parota', ['ngRoute',
                         'ngCookies',
                         'ngMessages',
                         'parota.navbar',
                         'parota.message',
                         'parota.registration',
                         'parota.login',
                         'parota.admin',
                         'parota.jobs'
                        ]);
})();
