(function() {
    'use strict';

    angular
        .module('parota')
        .factory('pollerService', pollerService);

    pollerService.$inject = ['$http', '$timeout'];

    function pollerService($http, $timeout) {
        var service = {
            getPoller: getPoller
        };

        return service;
        ///////////////

        function getPoller(url, terminationPredicate) {
            return function() {
                var data = {value: 0};
                var poller = () => {
                    if(terminationPredicate(data.value)) {
                        return;
                    }

                    $http.get(url).then((response) => {
                        data.value = parseInt(response.data);
                        $timeout(poller, 1000);
                    });
                };

                poller();
                return {
                    data: data
                };
            }();
        }
    }
})();