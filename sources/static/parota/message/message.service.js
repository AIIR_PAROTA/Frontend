(function() {
    'use strict';


    angular
        .module('parota.message')
        .factory('messageService', messageService);

    messageService.$inject = ['growl'];

    function messageService(growl) {
        var messages = [];

        var service = {
            displayError: displayError,
            displayInfo: displayInfo,
            clearAllMessages: clearAllMessages
        };

        return service;
        ///////////////

        function displayError(message) {
            return processMessage(growl.error(message));
        }

        function displayInfo(message) {
            return processMessage(growl.success(message));
        }

        function clearAllMessages() {
            messages.forEach((message) => message.destroy());
        }

        function processMessage(messageRef) {
            messages.push(messageRef);
            return messageRef;
        }

    }
})();