(function() {
    'use strict';

    angular
        .module('parota.jobs.jobDetails')
        .controller('JobDetailsController', JobDetailsController);

    JobDetailsController.$inject = ['job', 'pollerService'];

    function JobDetailsController(job, pollerService) {
        var vm = this;

        var POLLING_URL = '/api/jobs/' + job._id.$oid + '/progress';
        var TERMINATION_PREDICATE =  (progress) => {return progress === 100;};

        vm.isCollapsed = true;

        vm.job = job;

        vm.isDataReady = isDataReady;
        vm.downloadResultFile = downloadResultFile;
        vm.toggleUserOptions = toggleUserOptions;

        init();
        ///////////////

        function init() {
            vm.job.progress = pollerService.getPoller(POLLING_URL, TERMINATION_PREDICATE).data;
        }

        function isDataReady() {
            return vm.job.progress.value === 100;
        }

        function downloadResultFile() {
            window.open('/api/jobs/' + job._id.$oid + '/download');
        }

        function toggleUserOptions() {
            vm.isCollapsed = !vm.isCollapsed;
        }
    }
})();