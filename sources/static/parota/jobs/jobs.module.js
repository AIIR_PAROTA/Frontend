(function() {
    'use strict';

    angular
        .module('parota.jobs', ['parota.jobs.newJob',
                                'parota.jobs.showJobs',
                                'parota.jobs.jobDetails']);
})();