(function() {
    'use strict';

    angular
        .module('parota.jobs.showJobs')
        .controller('JobsController', JobsController);

    JobsController.$inject = ['allJobs', 'authenticationService', 'NgTableParams'];

    function JobsController(allJobs, authenticationService, NgTableParams) {
        var vm = this;

        vm.jobsTable = {};

        init();
        ///////////////

        function init() {
            allJobs.forEach((job) => {
               authenticationService.getUserById(job.user_id).then((response) => {
                    var userData = response.data;
                    job.owner = userData.firstname + ' ' + userData.lastname;
               });
            });
            initJobsTable(allJobs);
        }

        function initJobsTable(jobs) {
            var initialParameters = {
                count: 10
            };
            var settings = {
                data: jobs,
                counts: []
            };
            vm.jobsTable = new NgTableParams(initialParameters, settings);
        }
    }
})();