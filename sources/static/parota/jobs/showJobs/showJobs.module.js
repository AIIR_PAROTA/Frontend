(function() {
    'use strict';

    angular
        .module('parota.jobs.showJobs', ['ngTable']);
})();