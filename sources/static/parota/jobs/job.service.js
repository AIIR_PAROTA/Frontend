(function() {
    'use strict';

    angular
        .module('parota.jobs')
        .factory('jobService', jobService);

    jobService.$inject = ['$http', 'httpRequestHandlers'];

    function jobService($http, httpRequestHandlers) {
        var BASE_URL = '/api/jobs';

        var service = {
            getAllJobs: getAllJobs,
            getJobById: getJobById,
            createJob: createJob
        };

        return service;
        ////////////////

        function getAllJobs() {
            return $http.get(BASE_URL)
                .then(httpRequestHandlers.handleResponse())
                .catch(httpRequestHandlers.handleError({msg: "An error occurred while getting all jobs", shouldDisplayMessage: true}));
        }

        function getJobById(jobId) {
            return $http.get(BASE_URL + '/' + jobId)
                .then(httpRequestHandlers.handleResponse())
                .catch(httpRequestHandlers.handleError({msg: "An error occurred while getting job details", shouldDisplayMessage: true}));
        }

        function createJob(jobData) {
            return $http.post(BASE_URL, jobData)
                .then(httpRequestHandlers.handleResponse({msg: "A new job was scheduled", shouldDisplayMessage: true}))
                .catch(httpRequestHandlers.handleError({msg: "An error occurred while adding new job", shouldDisplayMessage: true}));
        }
    }
})();