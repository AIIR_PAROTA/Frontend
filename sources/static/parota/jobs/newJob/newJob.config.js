(function() {
    'use strict';
    angular
        .module('parota.jobs.newJob')
        .constant('newJobConfig', newJobConfig);

    newJobConfig.$inject = [];

    function newJobConfig() {
        return {
            "name" : "",
            "algorithm" : "HEU",
            /*"workers" : 1,
            "cooling" : "",
            "neighbourhood" : "",
            "iterations" : 200,*/
            "algorithms" : [
                {
                    "algorithm" : "HEU",
                    "text" : "Heuristic"
                },
                {
                    "algorithm" : "SA",
                    "text" : "Simulated Annealing"
                },
                {
                    "algorithm" : "G",
                    "text" : "Genetic Algorithm"
                }
            ],
            "functions_sa" : [
                "0.5", "0.6", "0.8", "0.9", "0.99", "magic-function"
            ],
            "neighbours_sizes" : [
                "n", "n^2", "n/2", "all"
            ]
        };
    }
})();
