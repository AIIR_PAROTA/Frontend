(function() {
    'use strict';

    angular
        .module('parota.jobs.newJob')
        .controller('NewJobController', NewJobController);

    NewJobController.$inject = ['$location', 'jobService', 'authenticationService', 'newJobConfig', 'Upload'];

    function NewJobController($location, jobService, authenticationService, newJobConfig, Upload) {
        var vm = this;

        vm.job = {};
        vm.excelFile = {};
        vm.addNewJob = addNewJob;

        init();
        ///////////////

        function init() {
            vm.job = newJobConfig();
        }

        function getNewJobData() {
            var newJob = {};
            newJob.job_name = vm.job.name;
            newJob.user_id = authenticationService.getUserData()._id.$oid;
            newJob.date = moment();
            /*newJob.cluster_config = {
                "workers" : vm.job.workers,
            };
            newJob.algorithm_config = {
                "algorithm" : vm.job.algorithm,
                "iterations" : vm.job.iterations,
                "specification" : {}
            };
            if (vm.job.algorithm === "SA") {
                newJob.algorithm_config.specification = {
                    "cooling" : vm.job.cooling,
                    "neighbourhood" : vm.job.neighbourhood
                };
            } else {
                // additional options for genetic algorithm
            }*/

            return newJob;
        }

        function addNewJob() {
            Upload.upload({url: '/api/jobs/upload', data: { file: vm.excelFile }})
            .then((response) => {
                var newJobData = getNewJobData();
                newJobData._id = response.data.$oid;

                jobService.createJob(newJobData).then((response) => {
                    $location.path('/job/' + response.data.job_id.$oid);
                 });
            });
        }
    }
})();