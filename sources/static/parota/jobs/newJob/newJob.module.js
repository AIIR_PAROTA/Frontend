(function() {
    'use strict';

    angular
        .module('parota.jobs.newJob', ['ngFileUpload']);
})();