(function() {
    'use strict';

    angular
        .module('parota.login')
        .controller('LoginController', LoginController);

    LoginController.$inject = ['authenticationService', '$location'];

    function LoginController(authenticationService, $location) {
        var vm = this;

        vm.loginForm = {};

        vm.login = login;

        init();
        ///////////////

        function init() {
            vm.loginForm.email = "";
            vm.loginForm.password = "";
        }

        function login(isValid) {
            if(isValid) {
                var credentials = {};
                credentials.email = vm.loginForm.email;
                credentials.password = vm.loginForm.password;

                authenticationService.login(credentials).then(() => {
                    authenticationService.getSessionUser().then((response) => {
                        authenticationService.setUserData(response.data);
                        $location.path('/');
                    });
                });
            }
        }
    }
})();