(function() {
    'use strict';

    angular
        .module('parota.login')
        .factory('authenticationService', authenticationService);

    authenticationService.$inject = ['$cookies', '$http', 'httpRequestHandlers'];

    function authenticationService($cookies, $http, httpRequestHandlers) {
        var COOKIE_KEY = "session";

        var userData = {};

        var service = {
            isAuthenticated: isAuthenticated,
            isAdmin: isAdmin,
            login: login,
            logout: logout,
            setUserData: setUserData,
            getUserData: getUserData,
            getSessionUser: getSessionUser,
            getUserById: getUserById
        };

        return service;
        ////////////////

        function isAuthenticated() {
            return !!$cookies.get(COOKIE_KEY);
        }

        function isAdmin() {
            return isAuthenticated() && userData.isAdmin;
        }

        function login(credentials) {
            return $http.post('/api/users/login', credentials)
                .then(httpRequestHandlers.handleResponse({shouldClearMessages: true}))
                .catch(httpRequestHandlers.handleError({msg: "Wrong username or password", shouldDisplayMessage: true}));
        }

        function logout() {
            $cookies.remove(COOKIE_KEY); // TODO investigate this bug (Flask-login not removing cookie on logout)

            return $http.post('/api/users/logout')
                .then(httpRequestHandlers.handleResponse({msg: "Successfully logged out", shouldDisplayMessage: true}))
                .catch(httpRequestHandlers.handleError({msg: "An error occurred while logging out", shouldDisplayMessage: true }));
        }

        function setUserData(data) {
            userData = data;
        }

        function getUserData() {
            return userData;
        }

        function getSessionUser() {
            return $http.get('/api/users/session')
                .then(httpRequestHandlers.handleResponse())
                .catch(httpRequestHandlers.handleError({msg: "An error occurred while getting user data", shouldDisplayMessage: true}));
        }

        function getUserById(id) {
            return $http.get('/api/users/' + id)
                .then(httpRequestHandlers.handleResponse())
                .catch(httpRequestHandlers.handleError({msg: "An error occurred while getting user with id " + id, shouldDisplayMessage: true}));
        }
    }
})();