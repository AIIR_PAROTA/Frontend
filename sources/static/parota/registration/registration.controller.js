(function() {
  'use strict';

  angular
      .module('parota.registration')
      .controller('RegistrationController', RegistrationController);

  RegistrationController.$inject = ['$location', 'registrationService'];

  function RegistrationController($location, registrationService) {
    var vm = this;

    vm.registerForm = {};

    vm.register = register;

    init();
    ///////////////

    function init() {
      vm.registerForm.email = "";
      vm.registerForm.firstname = "";
      vm.registerForm.lastname = "";
      vm.registerForm.password = "";
      vm.registerForm.confirmedPass = "";
    }

    function register(isFormValid) {
        console.log('Register data:', vm.registerForm);

        if (isFormValid && passwordsMatches()) {
            var data = {
                email: vm.registerForm.email,
                firstname: vm.registerForm.firstname,
                lastname: vm.registerForm.lastname,
                password: vm.registerForm.password,
                creationDate: new Date()
            };

            registrationService.register(data).then(() => {
                $location.url('/');
            });
        }

        function passwordsMatches() {
            return vm.registerForm.password === vm.registerForm.confirmedPass;
        }
    }
  }
})();
