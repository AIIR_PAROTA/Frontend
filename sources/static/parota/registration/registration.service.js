(function() {
  'use strict';

  angular
      .module('parota.registration')
      .factory('registrationService', registrationService);

  registrationService.$inject = ['$http', 'httpRequestHandlers'];

  function registrationService($http, httpRequestHandlers) {
    var service = {
      register: register
    };

    return service;
    ////////////////

    function register(data) {
        return $http.post('/api/users', data)
            .then(httpRequestHandlers.handleResponse({msg: 'Confirmation mail was sent', shouldDisplayMessage: true}))
            .catch(httpRequestHandlers.handleError({msg: 'An error occurred while creating account', shouldDisplayMessage: true}));
    }
  }
})();
