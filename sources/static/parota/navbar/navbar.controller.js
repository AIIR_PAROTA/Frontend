(function() {
    'use strict';

    angular
        .module('parota.navbar')
        .controller('NavbarController', NavbarController);

    NavbarController.$inject = ['$location', 'authenticationService'];

    function NavbarController($location, authenticationService) {
        var vm = this;

        vm.isAuthenticated = authenticationService.isAuthenticated;
        vm.isAdmin = authenticationService.isAdmin;
        vm.userData = authenticationService.getUserData;
        vm.goToLoginPage = goToLoginPage;
        vm.logout = logout;

        init();
        //////////////

        function init() {
            if(authenticationService.isAuthenticated()) {
                authenticationService.getSessionUser().then((response) => {
                    authenticationService.setUserData(response.data);
                });
            }
        }

        function goToLoginPage() {
            $location.path('/login');
        }

        function logout() {
            authenticationService.logout().then(() => {
                authenticationService.setUserData({});
                $location.path('/');
            });
        }
    }
})();