(function() {
    'use strict';

    angular
        .module('parota')
        .constant('routerConfig', routerConfig);

    routerConfig.$inject = ['$routeProvider'];

    function routerConfig($routeProvider) {
        $routeProvider
          .when('/', {
              templateUrl: 'static/parota/main/main.view.html'
          })

          .when('/register', {
              templateUrl: 'static/parota/registration/register.view.html',
              controller: 'RegistrationController',
              controllerAs: 'vm'
          })

          .when('/login', {
              templateUrl: 'static/parota/login/login.view.html',
              controller: 'LoginController',
              controllerAs: 'vm'
          })

          .when('/admin', {
              templateUrl: 'static/parota/admin/admin.view.html',
              controller: 'AdminController',
              controllerAs: 'vm'
          })

          .when('/create-job', {
              templateUrl: 'static/parota/jobs/newJob/newJob.view.html',
              controller: 'NewJobController',
              controllerAs: 'vm'
          })

          .when('/show-jobs', {
              templateUrl: 'static/parota/jobs/showJobs/showJobs.view.html',
              controller: 'JobsController',
              controllerAs: 'vm',
              resolve: {
                  allJobs : getAllJobs
              }
          })

          .when('/job/:jobId?', {
              templateUrl: 'static/parota/jobs/jobDetails/jobDetails.view.html',
              controller: 'JobDetailsController',
              controllerAs: 'vm',
              resolve: {
                  job: getJob
              }
          })

          .when('/error', {
              templateUrl: 'static/parota/common/error.view.html'
          })

          .otherwise({
              redirectTo: '/'
          });
    }

    function getJob($route, jobService, authenticationService) {
        var jobId = $route.current.params.jobId;
        return jobService.getJobById(jobId).then((job_response) => {
            var jobData = job_response.data;
            return authenticationService.getUserById(jobData.user_id).then((user_response) => {
                jobData.user = user_response.data;
                return jobData;
            });
        });
    }

    function getAllJobs($location, jobService) {
        return jobService.getAllJobs().then((response) => {
                return response.data;
        }).catch((error) => {
            $location.path('/error');
        });
    }
})();