(function() {
    'use strict';

    angular
        .module('parota')
        .constant('growlConfig', growlConfig);

    growlConfig.$inject = ['growlProvider'];

    function growlConfig(growlProvider) {
        var TTL = {
            success: 2000,
            error: -1,
            warning: 2000,
            info: 2000
        };

        growlProvider.onlyUniqueMessages(false);
        growlProvider.globalTimeToLive(TTL);
        growlProvider.globalDisableCountDown(true);
    }
})();