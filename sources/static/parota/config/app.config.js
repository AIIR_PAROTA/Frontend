(function(){
  'use strict';

  angular
      .module('parota')
      .config(config);

  config.$inject = ['$routeProvider', 'routerConfig',
                    '$locationProvider', 'locationProviderConfig',
                    'growlProvider', 'growlConfig'];

  function config($routeProvider, routerConfig,
                  $locationProvider, locationProviderConfig,
                  growlProvider, growlConfig) {
      routerConfig($routeProvider);
      locationProviderConfig($locationProvider);
      growlConfig(growlProvider);
  }
})();

