(function() {
    'use strict';

    angular
        .module('parota')
        .constant('locationProviderConfig', locationProviderConfig);

    locationProviderConfig.$inject = ['$locationProvider'];

    function locationProviderConfig($locationProvider) {
        $locationProvider.html5Mode(true);
        $locationProvider.hashPrefix('');
    }
})();