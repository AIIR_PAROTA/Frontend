# Frontend
1. Main goal
  * Web UI for PAROTA project
      * display rota
      * generate new rota
      * jobs view *(see progress / cancel job etc)*
2. Prerequisites
  * Java Script

How to setup docker:
1. Install virtualbox and docker-machine
2. Create a new machine called "aiir"
```
docker-machine create --driver virtualbox aiir
```
3. Change mounted directory:
 * Shutdown aiir machine `docker-machine stop aiir`
 * Open Virtualbox, choose aiir machine, go to Settings, Shared Folders
 * Remove default mounted directory
 * Add main project directory (the directory containing all subprojects: Frontend, Server, Cluster)
 * Don't forget to tick the 'Auto-mount' option
 * Start machine again `docker-machine start aiir`
 * Check if directory was mounted properly
 ```
 docker-machine ssh aiir
 ls /
 ```

How to setup frontend:
1. Ssh into aiir machine `docker-machine ssh aiir`
2. Install `docker-compose` using the following commands:
    `url="https://github.com/docker/compose/releases/download/1.11.2/docker-compose-$(uname -s)-$(uname -m)";
     docker_compose_dest="/usr/local/bin/docker-compose";
     sudo curl -L ${url} -o ${docker_compose_dest};
     sudo chmod +x ${docker_compose_dest};`
3. Install project dependencies `docker-compose run --rm devtools bower --allow-root install` and wait for finish
4. Run nginx (server) container `docker-compose up -d nginx`
